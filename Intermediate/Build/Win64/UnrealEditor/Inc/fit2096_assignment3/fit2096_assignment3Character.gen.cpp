// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "fit2096_assignment3/fit2096_assignment3Character.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodefit2096_assignment3Character() {}
// Cross Module References
	FIT2096_ASSIGNMENT3_API UClass* Z_Construct_UClass_Afit2096_assignment3Character_NoRegister();
	FIT2096_ASSIGNMENT3_API UClass* Z_Construct_UClass_Afit2096_assignment3Character();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_fit2096_assignment3();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	FIT2096_ASSIGNMENT3_API UClass* Z_Construct_UClass_ARock_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(Afit2096_assignment3Character::execThrowRock)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ThrowRock();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(Afit2096_assignment3Character::execResetSpeed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetSpeed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(Afit2096_assignment3Character::execAccelerate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Accelerate();
		P_NATIVE_END;
	}
	void Afit2096_assignment3Character::StaticRegisterNativesAfit2096_assignment3Character()
	{
		UClass* Class = Afit2096_assignment3Character::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Accelerate", &Afit2096_assignment3Character::execAccelerate },
			{ "ResetSpeed", &Afit2096_assignment3Character::execResetSpeed },
			{ "ThrowRock", &Afit2096_assignment3Character::execThrowRock },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Increase speed **/" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
		{ "ToolTip", "Increase speed *" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_Afit2096_assignment3Character, nullptr, "Accelerate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Reset speed **/" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
		{ "ToolTip", "Reset speed *" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_Afit2096_assignment3Character, nullptr, "ResetSpeed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_Afit2096_assignment3Character, nullptr, "ThrowRock", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(Afit2096_assignment3Character);
	UClass* Z_Construct_UClass_Afit2096_assignment3Character_NoRegister()
	{
		return Afit2096_assignment3Character::StaticClass();
	}
	struct Z_Construct_UClass_Afit2096_assignment3Character_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TurnRateGamepad_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TurnRateGamepad;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RockClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_RockClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Afit2096_assignment3Character_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_fit2096_assignment3,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_Afit2096_assignment3Character_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_Afit2096_assignment3Character_Accelerate, "Accelerate" }, // 2369773815
		{ &Z_Construct_UFunction_Afit2096_assignment3Character_ResetSpeed, "ResetSpeed" }, // 3094784957
		{ &Z_Construct_UFunction_Afit2096_assignment3Character_ThrowRock, "ThrowRock" }, // 2361415853
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afit2096_assignment3Character_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "fit2096_assignment3Character.h" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera behind the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Afit2096_assignment3Character, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Follow camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Afit2096_assignment3Character, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_FollowCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_TurnRateGamepad_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_TurnRateGamepad = { "TurnRateGamepad", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Afit2096_assignment3Character, TurnRateGamepad), METADATA_PARAMS(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_TurnRateGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_TurnRateGamepad_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_RockClass_MetaData[] = {
		{ "Category", "Rock" },
		{ "ModuleRelativePath", "fit2096_assignment3Character.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_RockClass = { "RockClass", nullptr, (EPropertyFlags)0x0014000000000001, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Afit2096_assignment3Character, RockClass), Z_Construct_UClass_ARock_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_RockClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_RockClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_Afit2096_assignment3Character_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_CameraBoom,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_FollowCamera,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_TurnRateGamepad,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_Afit2096_assignment3Character_Statics::NewProp_RockClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_Afit2096_assignment3Character_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Afit2096_assignment3Character>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_Afit2096_assignment3Character_Statics::ClassParams = {
		&Afit2096_assignment3Character::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_Afit2096_assignment3Character_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3Character_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_Afit2096_assignment3Character_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3Character_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Afit2096_assignment3Character()
	{
		if (!Z_Registration_Info_UClass_Afit2096_assignment3Character.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_Afit2096_assignment3Character.OuterSingleton, Z_Construct_UClass_Afit2096_assignment3Character_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_Afit2096_assignment3Character.OuterSingleton;
	}
	template<> FIT2096_ASSIGNMENT3_API UClass* StaticClass<Afit2096_assignment3Character>()
	{
		return Afit2096_assignment3Character::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(Afit2096_assignment3Character);
	struct Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_Afit2096_assignment3Character, Afit2096_assignment3Character::StaticClass, TEXT("Afit2096_assignment3Character"), &Z_Registration_Info_UClass_Afit2096_assignment3Character, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(Afit2096_assignment3Character), 114226199U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_1341754576(TEXT("/Script/fit2096_assignment3"),
		Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
