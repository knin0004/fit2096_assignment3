// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "fit2096_assignment3/fit2096_assignment3GameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodefit2096_assignment3GameMode() {}
// Cross Module References
	FIT2096_ASSIGNMENT3_API UClass* Z_Construct_UClass_Afit2096_assignment3GameMode_NoRegister();
	FIT2096_ASSIGNMENT3_API UClass* Z_Construct_UClass_Afit2096_assignment3GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_fit2096_assignment3();
// End Cross Module References
	void Afit2096_assignment3GameMode::StaticRegisterNativesAfit2096_assignment3GameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(Afit2096_assignment3GameMode);
	UClass* Z_Construct_UClass_Afit2096_assignment3GameMode_NoRegister()
	{
		return Afit2096_assignment3GameMode::StaticClass();
	}
	struct Z_Construct_UClass_Afit2096_assignment3GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_fit2096_assignment3,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "fit2096_assignment3GameMode.h" },
		{ "ModuleRelativePath", "fit2096_assignment3GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Afit2096_assignment3GameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::ClassParams = {
		&Afit2096_assignment3GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Afit2096_assignment3GameMode()
	{
		if (!Z_Registration_Info_UClass_Afit2096_assignment3GameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_Afit2096_assignment3GameMode.OuterSingleton, Z_Construct_UClass_Afit2096_assignment3GameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_Afit2096_assignment3GameMode.OuterSingleton;
	}
	template<> FIT2096_ASSIGNMENT3_API UClass* StaticClass<Afit2096_assignment3GameMode>()
	{
		return Afit2096_assignment3GameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(Afit2096_assignment3GameMode);
	struct Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3GameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3GameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_Afit2096_assignment3GameMode, Afit2096_assignment3GameMode::StaticClass, TEXT("Afit2096_assignment3GameMode"), &Z_Registration_Info_UClass_Afit2096_assignment3GameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(Afit2096_assignment3GameMode), 2898521092U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3GameMode_h_2466572825(TEXT("/Script/fit2096_assignment3"),
		Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3GameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3GameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
