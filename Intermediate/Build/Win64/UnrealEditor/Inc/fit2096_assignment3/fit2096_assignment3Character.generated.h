// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2096_ASSIGNMENT3_fit2096_assignment3Character_generated_h
#error "fit2096_assignment3Character.generated.h already included, missing '#pragma once' in fit2096_assignment3Character.h"
#endif
#define FIT2096_ASSIGNMENT3_fit2096_assignment3Character_generated_h

#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_SPARSE_DATA
#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execThrowRock); \
	DECLARE_FUNCTION(execResetSpeed); \
	DECLARE_FUNCTION(execAccelerate);


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execThrowRock); \
	DECLARE_FUNCTION(execResetSpeed); \
	DECLARE_FUNCTION(execAccelerate);


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAfit2096_assignment3Character(); \
	friend struct Z_Construct_UClass_Afit2096_assignment3Character_Statics; \
public: \
	DECLARE_CLASS(Afit2096_assignment3Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/fit2096_assignment3"), NO_API) \
	DECLARE_SERIALIZER(Afit2096_assignment3Character)


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAfit2096_assignment3Character(); \
	friend struct Z_Construct_UClass_Afit2096_assignment3Character_Statics; \
public: \
	DECLARE_CLASS(Afit2096_assignment3Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/fit2096_assignment3"), NO_API) \
	DECLARE_SERIALIZER(Afit2096_assignment3Character)


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Afit2096_assignment3Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Afit2096_assignment3Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Afit2096_assignment3Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Afit2096_assignment3Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Afit2096_assignment3Character(Afit2096_assignment3Character&&); \
	NO_API Afit2096_assignment3Character(const Afit2096_assignment3Character&); \
public:


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Afit2096_assignment3Character(Afit2096_assignment3Character&&); \
	NO_API Afit2096_assignment3Character(const Afit2096_assignment3Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Afit2096_assignment3Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Afit2096_assignment3Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Afit2096_assignment3Character)


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_9_PROLOG
#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_SPARSE_DATA \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_RPC_WRAPPERS \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_INCLASS \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_SPARSE_DATA \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_INCLASS_NO_PURE_DECLS \
	FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT2096_ASSIGNMENT3_API UClass* StaticClass<class Afit2096_assignment3Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_fit2096_assignment3_Source_fit2096_assignment3_fit2096_assignment3Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
