// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodefit2096_assignment3_init() {}
	FIT2096_ASSIGNMENT3_API UFunction* Z_Construct_UDelegateFunction_ASentryController_OnPlayerDetected__DelegateSignature();
	FIT2096_ASSIGNMENT3_API UFunction* Z_Construct_UDelegateFunction_fit2096_assignment3_OnEndPadActivated__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_fit2096_assignment3;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_fit2096_assignment3()
	{
		if (!Z_Registration_Info_UPackage__Script_fit2096_assignment3.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_ASentryController_OnPlayerDetected__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_fit2096_assignment3_OnEndPadActivated__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/fit2096_assignment3",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x05B95EB7,
				0x8409CBCA,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_fit2096_assignment3.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_fit2096_assignment3.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_fit2096_assignment3(Z_Construct_UPackage__Script_fit2096_assignment3, TEXT("/Script/fit2096_assignment3"), Z_Registration_Info_UPackage__Script_fit2096_assignment3, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x05B95EB7, 0x8409CBCA));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
