// Copyright Epic Games, Inc. All Rights Reserved.

#include "fit2096_assignment3GameMode.h"
#include "fit2096_assignment3Character.h"
#include "UObject/ConstructorHelpers.h"

Afit2096_assignment3GameMode::Afit2096_assignment3GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
