// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "fit2096_assignment3/fit2096_assignment3Character.h"
#include "GameFramework/Pawn.h"
#include "Perception/PawnSensingComponent.h"
#include "Sentry.generated.h"

UCLASS()
class FIT2096_ASSIGNMENT3_API ASentry : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASentry();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* SentryMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
	UMaterialInterface* material;

	Afit2096_assignment3Character* player;
	UMaterialInstanceDynamic* matInstance;

	float RotateSpeed = 20;
	float RotationAngle = 45;
	FVector StartDirection;
	FVector TargetDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// UFUNCTION()
	// void OnPlayerDetected();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = SENTRY)
	UPawnSensingComponent* SensingConfiguration;

};
