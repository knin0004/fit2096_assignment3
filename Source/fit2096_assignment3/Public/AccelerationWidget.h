// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "fit2096_assignment3/fit2096_assignment3Character.h"
#include "AccelerationWidget.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class FIT2096_ASSIGNMENT3_API UAccelerationWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	Afit2096_assignment3Character* Player;
	void NativeConstruct() override;
	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* AccelerationLabel;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ThrowLabel;
};
