// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Sentry.h"
#include "SentryController.generated.h"

/**
 * 
 */
UCLASS()
class FIT2096_ASSIGNMENT3_API ASentryController : public AAIController
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerDetected);

public:
	ASentryController();
	
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void OnUnPossess() override;

	UPROPERTY(BlueprintAssignable)
	FOnPlayerDetected PlayerDetectedDelegate;
	
protected:
	ASentry* PossessedSentry;
};
