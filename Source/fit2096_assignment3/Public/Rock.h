// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "fit2096_assignment3/fit2096_assignment3Character.h"
#include "GameFramework/Actor.h"
#include "Rock.generated.h"

UCLASS()
class FIT2096_ASSIGNMENT3_API ARock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARock();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* RockMesh;

	UPROPERTY(EditAnywhere)
	USphereComponent* RockHitBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
	UMaterialInterface* material;
	
	UMaterialInstanceDynamic* matInstance;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Distraction")
	float sweepSize = 300.f;

	UPROPERTY(EditAnywhere)
	float MaxLifeTime = 5;

	UPROPERTY(EditAnywhere)
	float CurrentLifeTime = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// UFUNCTION()
	// void OnRockHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};
