// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Enemy.h"
#include "NavigationSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "RoamEnemyController.generated.h"

/**
 * 
 */
UCLASS()
class FIT2096_ASSIGNMENT3_API ARoamEnemyController : public AAIController
{
	GENERATED_BODY()

public:
	ARoamEnemyController();

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual FRotator GetControlRotation() const override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	UNavigationSystemV1* NavigationSystem;

protected:
	//AEnemy* PossessedEnemy;
};
