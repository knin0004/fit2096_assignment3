// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "fit2096_assignment3/fit2096_assignment3Character.h"
#include "GameFramework/Actor.h"
#include "EndPad.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndPadActivated);

UCLASS()
class FIT2096_ASSIGNMENT3_API AEndPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEndPad();

	UPROPERTY(BlueprintAssignable)
	FOnEndPadActivated EndPadHitBoxDelegate;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* EndPadMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent* EndPadHitBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
	UMaterialInterface* material;

	Afit2096_assignment3Character* player;
	UMaterialInstanceDynamic* matInstance;

	UFUNCTION()
	void OnHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
