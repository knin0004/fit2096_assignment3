// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "fit2096_assignment3GameMode.generated.h"

UCLASS(minimalapi)
class Afit2096_assignment3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Afit2096_assignment3GameMode();
};



