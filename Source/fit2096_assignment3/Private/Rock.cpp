// Fill out your copyright notice in the Description page of Project Settings.


#include "Rock.h"

// Sets default values
ARock::ARock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RockHitBox = CreateDefaultSubobject<USphereComponent>(TEXT("Rock HitBox"));
	// RockHitBox->OnComponentBeginOverlap.AddDynamic(this, &ARock::OnRockHit);
	// RockHitBox->OnComponentHit.AddDynamic(this, &ARock::OnRockHit);
	RootComponent = RockHitBox;
	
	RockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Rock Mesh"));
	// RockMesh->OnComponentHit.AddDynamic(this, &ARock::OnRockHit);
	RockMesh->SetupAttachment(RootComponent);
	// RootComponent = RockMesh;
	// RockMesh->SetupAttachment(RockHitBox);

	// OnActorHit.AddDynamic(this, &ARock::OnRockHit);
}



// Called when the game starts or when spawned
void ARock::BeginPlay()
{
	Super::BeginPlay();

	if (material)
	{
		matInstance = UMaterialInstanceDynamic::Create(material, this);
		RockMesh->SetMaterial(0, matInstance);
	}
}

// Called every frame
void ARock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentLifeTime += DeltaTime;
	if (CurrentLifeTime >= MaxLifeTime)
	{
		Destroy();
	}
}

// void ARock::OnRockHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
// {
// 	UE_LOG(LogTemp, Warning, TEXT("Rock Hit"));
// 	
// 	// Rock glow
	// matInstance->SetScalarParameterValue("EmiColor", 20);
	
	// TArray<FHitResult> OutHits;
	// FVector location = GetActorLocation();
	// FCollisionShape ExplosionSphere = FCollisionShape::MakeSphere(sweepSize);
	// DrawDebugSphere(GetWorld(), location, ExplosionSphere.GetSphereRadius(), 50, FColor::Red, false, 1.0f, 0,0);
	// if(GetWorld()->SweepMultiByChannel(OutHits, location,location, FQuat::Identity, ECC_WorldStatic, ExplosionSphere)){
	// 	for(auto& H : OutHits){
	// 		Afit2096_assignment3Character* Enemy = Cast<Afit2096_assignment3Character>(H.GetActor());
	// 		if (Enemy)
	// 		{
	// 			UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(Enemy->GetRootComponent());
	// 			Mesh->AddRadialImpulse(location, sweepSize, -300.0f, ERadialImpulseFalloff::RIF_Linear, true);
	// 		}
	// 	}
	// }
// }
