// Fill out your copyright notice in the Description page of Project Settings.


#include "Sentry.h"

#include "SAdvancedTransformInputBox.h"

// Sets default values
ASentry::ASentry()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SentryMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	SentryMesh->SetupAttachment(RootComponent);

	SensingConfiguration = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensing Configuration"));
	SensingConfiguration->SightRadius = 1000;
	SensingConfiguration->SetPeripheralVisionAngle(45);
}

// Called when the game starts or when spawned
void ASentry::BeginPlay()
{
	Super::BeginPlay();

	StartDirection = GetActorForwardVector();
	StartDirection.Normalize();
	//UE_LOG(LogTemp, Warning, TEXT("Facing: %s"), *StartDirection.ToString());

	player = Cast<Afit2096_assignment3Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (material)
	{
		matInstance = UMaterialInstanceDynamic::Create(material, this);
		SentryMesh->SetMaterial(0, matInstance);
	}
	
}

// Called every frame
void ASentry::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector CurrentDirection = GetActorForwardVector();
	CurrentDirection.Normalize();
	float Angle = FMath::RadiansToDegrees(acosf(FVector::DotProduct(CurrentDirection, StartDirection)));
	if (Angle >= RotationAngle)
	{
		RotateSpeed = -RotateSpeed;
	}
	
	FRotator RotationAmount = FRotator(0, RotateSpeed * DeltaTime, 0);
	AddActorLocalRotation(RotationAmount);

	// Dynamic material
	if (player && matInstance)
	{
		float distanceToPlayer = FVector::Distance(player->GetActorLocation(), GetActorLocation());
		UE_LOG(LogTemp, Warning, TEXT("Dist: %f"), distanceToPlayer);

		matInstance->SetVectorParameterValue("Color", FLinearColor(1 - distanceToPlayer/1000, 0, distanceToPlayer/1000));
		matInstance->SetScalarParameterValue("Specular", 1-distanceToPlayer/1000);
	}
}

// Called to bind functionality to input
void ASentry::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

