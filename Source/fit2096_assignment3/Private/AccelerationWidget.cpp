// Fill out your copyright notice in the Description page of Project Settings.


#include "AccelerationWidget.h"
#include "Components/TextBlock.h"

void UAccelerationWidget::NativeConstruct()
{
	Super::NativeConstruct();
	Player = Cast<Afit2096_assignment3Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
}

void UAccelerationWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (Player)
	{
		FNumberFormattingOptions Opts;
		Opts.SetMaximumFractionalDigits(0);
		AccelerationLabel->SetText(FText::AsNumber(Player->AccelerationNum, &Opts));
		ThrowLabel->SetText(FText::AsNumber(Player->ThrowNum, &Opts));
	}
}
