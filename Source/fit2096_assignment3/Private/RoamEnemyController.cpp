// Fill out your copyright notice in the Description page of Project Settings.


#include "RoamEnemyController.h"

ARoamEnemyController::ARoamEnemyController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARoamEnemyController::BeginPlay()
{
	Super::BeginPlay();

	NavigationSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
	if (NavigationSystem)
	{
		FNavLocation ReturnLocation;
		NavigationSystem->GetRandomPointInNavigableRadius(GetPawn()->GetActorLocation(), 1000, ReturnLocation);
		MoveToLocation(ReturnLocation.Location);
	}
}

void ARoamEnemyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	//PossessedEnemy = Cast<AEnemy>(InPawn);
}

void ARoamEnemyController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

FRotator ARoamEnemyController::GetControlRotation() const
{
	if (GetPawn())
	{
		return FRotator(0, GetPawn()->GetActorRotation().Yaw, 0);
	}
	return FRotator::ZeroRotator;
}

void ARoamEnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	if (NavigationSystem)
	{
		FNavLocation ReturnLocation;
		NavigationSystem->GetRandomPointInNavigableRadius(GetPawn()->GetActorLocation(), 1000, ReturnLocation);
		MoveToLocation(ReturnLocation.Location);
	}
}
