// Fill out your copyright notice in the Description page of Project Settings.


#include "EndPad.h"

// Sets default values
AEndPad::AEndPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	EndPadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	EndPadMesh->SetupAttachment(RootComponent);

	EndPadHitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	EndPadHitBox->SetupAttachment(RootComponent);

	EndPadHitBox->OnComponentBeginOverlap.AddDynamic(this, &AEndPad::OnHitBoxOverlapBegin);
}

void AEndPad::OnHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player overlap EndPad"));
		EndPadHitBoxDelegate.Broadcast();
	}
}

// Called when the game starts or when spawned
void AEndPad::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<Afit2096_assignment3Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (material)
	{
		matInstance = UMaterialInstanceDynamic::Create(material, this);
		EndPadMesh->SetMaterial(0, matInstance);
	}
}

// Called every frame
void AEndPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Dynamic material
	if (player && matInstance)
	{
		float distanceToPlayer = FVector::Distance(player->GetActorLocation(), GetActorLocation());
		// UE_LOG(LogTemp, Warning, TEXT("Dist: %f"), distanceToPlayer);
		
		matInstance->SetScalarParameterValue("EmiColor", (1000-distanceToPlayer)*(distanceToPlayer/5000));
	}
}

