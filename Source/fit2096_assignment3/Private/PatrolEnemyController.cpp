// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolEnemyController.h"

#include "GameFramework/CharacterMovementComponent.h"

APatrolEnemyController::APatrolEnemyController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void APatrolEnemyController::BeginPlay()
{
	Super::BeginPlay();

	NavigationSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
	if (NavigationSystem)
	{
		GetNextLocation();
	}
}

void APatrolEnemyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	PossessedEnemy = Cast<AEnemy>(InPawn);
	
	if (PossessedEnemy && PossessedEnemy->PatrolPointsArray.Num() > 1)
	{
		PossessedEnemy->SetActorLocation(PossessedEnemy->PatrolPointsArray[0]);
	}
}

FRotator APatrolEnemyController::GetControlRotation() const
{
	if (GetPawn())
	{
		return FRotator(0, GetPawn()->GetActorRotation().Yaw, 0);
	}
	return Super::GetControlRotation();
}

void APatrolEnemyController::OnUnPossess()
{
	Super::OnUnPossess();

	PossessedEnemy = nullptr;
}

void APatrolEnemyController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void APatrolEnemyController::GetNextLocation()
{
	if (PossessedEnemy->PatrolPointsArray.Num() > 1)
	{
		Index = (Index == PossessedEnemy->PatrolPointsArray.Num()-1) ? 0 : Index + 1;

		GoalLocation =  PossessedEnemy->PatrolPointsArray[Index];
		
		MoveToLocation(GoalLocation);
	}
}

void APatrolEnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	if (NavigationSystem)
	{
		GetNextLocation();
	}
}

