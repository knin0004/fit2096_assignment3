// Fill out your copyright notice in the Description page of Project Settings.


#include "SentryController.h"

ASentryController::ASentryController()
{
}

void ASentryController::BeginPlay()
{
	Super::BeginPlay();
}

void ASentryController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	PossessedSentry = Cast<ASentry>(InPawn);
}

void ASentryController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	// FVector CurrentDirection = PossessedSentry->GetActorForwardVector();
	// CurrentDirection.Normalize();
	// float Angle = FMath::RadiansToDegrees(acosf(FVector::DotProduct(CurrentDirection, PossessedSentry->StartDirection)));
	// if (Angle >= PossessedSentry->RotationAngle)
	// {
	// 	PossessedSentry->RotateSpeed = -PossessedSentry->RotateSpeed;
	// }
	//
	// FRotator RotationAmount = FRotator(0, PossessedSentry->RotateSpeed * DeltaSeconds, 0);
	// PossessedSentry->AddActorLocalRotation(RotationAmount);
}

void ASentryController::OnUnPossess()
{
	Super::OnUnPossess();
}
