// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class fit2096_assignment3 : ModuleRules
{
	public fit2096_assignment3(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay",
			"NavigationSystem", "GameplayTasks"
		});
	}
}
